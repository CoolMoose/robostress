from robobrowser import RoboBrowser

browser = RoboBrowser()
browser.open('http://localhost:5000/errorTestResult')
goodResult = browser.response
goodParsed = browser.parsed
browser.open('http://localhost:5000/errorTestGood')
goodError = browser.response
errParsed = browser.parsed
browser.open('http://localhost:5000/test')
testedRes = browser.response
testedParsed = browser.parsed


print "Expected Result: ", goodResult, goodParsed

print "Expected Error: ", goodError, errParsed

print "Bad Error: ", testedRes, testedParsed

if goodError != testedRes:
    print "Bad Error was thrown!"