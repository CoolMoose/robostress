from robobrowser import RoboBrowser
import thread
import psutil
import json
from flask import Flask, request, render_template
app = Flask(__name__)

browsers = []
browserCount = 10
browserRequestCounts =[]
totalRequestCount = 0
browserRunning = []

# callCount = 0
# i = 0
# while i < 10:
#     browsers.append(RoboBrowser())
#     i += 1
#
# while True:
#     i = 0
#     while i < 10:
#         browsers[i].open('http://localhost/testJob')
#         callCount += 1
#         print callCount


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/errorTestResult')
def errorGood():
    return "Ok", 200, {'Content-Type': 'text/css; charset=utf-8'}


@app.route('/errorTestGood')
def error404():
    return "Error Not Found", 401, {'Content-Type': 'text/css; charset=utf-8'}


@app.route('/stopJob', methods=['POST'])
def stopJob():
    global browsers;
    global browserRequestCounts
    global browserCount
    global totalRequestCount
    global browserRunning

    browsers = []
    browserCount = 10
    browserRequestCounts =[]
    totalRequestCount = 0
    browserRunning = []


    return "Ok", 200, {'Content-Type': 'text/css; charset=utf-8'}


@app.route('/update', methods=['POST'])
def getUpdate():
    data = {}
    data['cpu'] = psutil.cpu_percent()
    data['mem'] = psutil.virtual_memory().percent
    global browserRequestCounts
    data['counts'] = browserRequestCounts

    return json.dumps(data), 200, {'Content-Type': 'json'}

@app.route('/testJob')
def testJob():
    return "Ok", 200, {'Content-Type': 'text/css; charset=utf-8'}


@app.route('/submitJob', methods=['POST'])
def submitJob():
    reqUrl = request.form['url']
    reqCount = request.form['count']
    global browserCount
    browserCount = int(reqCount)

    try:
        i = 0
        while i < browserCount:
            browsers.append(CustomBrowser(i))
            browserRequestCounts.append(0)
            browserRunning.append(1)
            i += 1

        i = 0
        while i < browserCount:
            browsers[i].makeFetchesAsync(reqUrl)
            i += 1

        print 'Job Started'
    except:
        print "Error: unable to start thread"

    return "Ok", 200, {'Content-Type': 'text/css; charset=utf-8'}


class CustomBrowser:
    b = RoboBrowser()
    name = ""

    def __init__(self, browserName):
        self.name = browserName

    def fetchThread(self, fetchUrl):
        global totalRequestCount
        global browserRequestCounts
        global browserRunning

        while browserRunning[self.name]:
            self.b.open(fetchUrl)
            browserRequestCounts[self.name] += 1
            totalRequestCount += 1

    def makeFetchesAsync(self, fetchUrl):
        thread.start_new_thread(self.fetchThread, (fetchUrl,))


if __name__ == "__main__":
    app.run(debug=True)






